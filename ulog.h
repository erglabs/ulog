#ifndef __ERGLABS_FOSS_ULOG__
#define __ERGLABS_FOSS_ULOG__
/* Copyright 2019 deathware.org
 * Contributors and and copyright holders of this project
 * owners are listed in Contributors.nfo file

 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 * copy of this license is distibuted in
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
// linux stuff
#include <sys/time.h>
#include <stdio.h>
#include <unistd.h>

// config!
// this should be changed if needed
static FILE * DefaultUlogSink = NULL;

static const uint8_t MaxLogLevel = 5;
static const char LOGLEVEL[6][5] = { "CRIT", "ERRO", "WARN", "TRCE", "INFO", "NOTE" };
static const uint8_t UlogCritical = 0;
static const uint8_t UlogError    = 1;
static const uint8_t UlogWarning  = 2;
static const uint8_t UlogTrace    = 3;
static const uint8_t UlogInfo     = 4;
static const uint8_t UlogNote     = 5;

// fallback to default precision if unspecified
#ifndef ULOG_PRECISION
    #define ULOG_PRECISION 9
#endif //ULOG_PRECISION

static void ulog(uint8_t level, FILE * sink, const char *fmt, ...)
{
    if (level > MaxLogLevel) level = MaxLogLevel;
    /* Get current time */
    struct timespec otimespec;
    clock_gettime(CLOCK_REALTIME, &otimespec);
    struct tm * otm = gmtime(&(otimespec.tv_sec));
    uint32_t ns = otimespec.tv_nsec;
    char nss[10];
    snprintf(nss,10,"%.09d",ns);
    va_list args;

    char timebuffer[20];
    timebuffer[20]='\0';
    strftime(timebuffer, 20, "%Y-%m-%d %H:%M:%S", otm);

    uint64_t nfmts = strlen(fmt) + 1;
    char nfmt[nfmts];
    memcpy(nfmt, fmt, nfmts - 1);
    nfmt[nfmts - 1] = '\n';
    nfmt[nfmts] =   '\0';

    va_start(args, fmt);
    fprintf(sink, "[%.19s::%.*s][%.5s] ", timebuffer, ULOG_PRECISION, nss, LOGLEVEL[level]);
    vfprintf(sink, nfmt, args);
    va_end(args);
}

static void ulogd(uint8_t level, FILE * sink, const char *fmt, va_list args)
{
    if (level > MaxLogLevel) level = MaxLogLevel;
    /* Get current time */
    struct timespec otimespec;
    clock_gettime(CLOCK_REALTIME, &otimespec);
    struct tm * otm = gmtime(&(otimespec.tv_sec));
    uint32_t ns = otimespec.tv_nsec;
    char nss[10];
    snprintf(nss,10,"%.09d",ns);


    char timebuffer[20];
    timebuffer[20]='\0';
    strftime(timebuffer, 20, "%Y-%m-%d %H:%M:%S", otm);

    uint64_t nfmts = strlen(fmt) + 1;
    char nfmt[nfmts];
    memcpy(nfmt, fmt, nfmts - 1);
    nfmt[nfmts - 1] = '\n';
    nfmt[nfmts] =   '\0';

    fprintf(sink, "[%.19s::%.*s][%.5s] ", timebuffer, ULOG_PRECISION, nss, LOGLEVEL[level]);
    vfprintf(sink, nfmt, args);
}

static void ulog_critical(const char * fmt, ...)
{
    if (DefaultUlogSink==NULL) DefaultUlogSink = stderr;
    va_list argw;
    va_start(argw, fmt);
    ulogd(UlogCritical, DefaultUlogSink, fmt, argw);
    va_end(argw);
}

static void ulog_error(const char * fmt, ...)
{
    if (DefaultUlogSink==NULL) DefaultUlogSink = stderr;
    va_list argw;
    va_start(argw, fmt);
    ulogd(UlogError, DefaultUlogSink, fmt, argw);
    va_end(argw);
}

static void ulog_warning(const char * fmt, ...)
{
    if (DefaultUlogSink==NULL) DefaultUlogSink = stderr;
    va_list argw;
    va_start(argw, fmt);
    ulogd(UlogWarning, DefaultUlogSink, fmt, argw);
    va_end(argw);
}

static void ulog_trace(const char * fmt, ...)
{
    if (DefaultUlogSink==NULL) DefaultUlogSink = stderr;
    va_list argw;
    va_start(argw, fmt);
    ulogd(UlogTrace, DefaultUlogSink, fmt, argw);
    va_end(argw);
}

static void ulog_info(const char * fmt, ...)
{
    if (DefaultUlogSink==NULL) DefaultUlogSink = stderr;
    va_list argw;
    va_start(argw, fmt);
    ulogd(UlogInfo, DefaultUlogSink, fmt, argw);
    va_end(argw);
}

static void ulog_note(const char * fmt, ...)
{
    if (DefaultUlogSink==NULL) DefaultUlogSink = stderr;
    va_list argw;
    va_start(argw, fmt);
    ulogd(UlogCritical, DefaultUlogSink, fmt, argw);
    va_end(argw);
}
#endif //__ERGLABS_FOSS_ULOG__
